#!/bin/bash
cd `dirname $BASH_SOURCE`

if [ ! -f index.pid ]
then
    exit
fi

pid=`cat index.pid`

if ps $pid > /dev/null
then
    echo "INFO Killing $pid"
    kill $pid
    while ps $pid > /dev/null
    do
        sleep 0.2
    done
fi

./clean.sh
