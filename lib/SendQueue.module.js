module.exports = app => () => {

    function next () {
        if (minutes.length === 0) return
        if (busy) return
        busy = true
        ;(log => {
            log.info('Begin')
            ;(done => {
                ;(error => {
                    app.HttpPostJson({
                        scheme: parsed_office.protocol === 'https:' ? 'https' : 'http',
                        host: parsed_office.hostname,
                        port: parsed_office.port,
                        path: parsed_office.pathname + 'api/put/',
                    }, {
                        username: app.config.office.username,
                        password: app.config.office.password,
                        minute: minutes.shift(),
                    }, response => {

                        if (response === 'INVALID_LOGIN') {
                            error({ code: 'INVALID_LOGIN' })
                            return
                        }

                        if (response === true) {
                            log.info('End')
                            done()
                            return
                        }

                        error({ code: 'INVALID_RESPONSE' })

                    }, error)
                })(err => {
                    log.info('Fail', err.code)
                    done()
                })
            })(() => {
                busy = false
                next()
            })
        })(log.sublog('Send'))
    }

    const parsed_office = require('url').parse(app.config.office.base)

    const log = app.log.sublog('SendQueue')

    let busy = false
    const minutes = []

    return minute => {
        minutes.push(minute)
        next()
    }

}
