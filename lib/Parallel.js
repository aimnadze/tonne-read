module.exports = (tasks, done) => {

    const results = tasks instanceof Array ? [] : {}
    const keys = Object.keys(tasks)
    if (keys.length === 0) {
        process.nextTick(() => {
            done(results)
        })
        return
    }

    let num_done = 0
    keys.forEach(key => {
        process.nextTick(() => {
            tasks[key](result => {
                results[key] = result
                num_done++
                if (num_done === keys.length) done(results)
            })
        })
    })

}
